/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"log"

	"github.com/go-logr/logr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	gitopsv1alpha1 "gitlab.com/sailr/opensource/scout/api/v1alpha1"
	"gitlab.com/sailr/opensource/scout/pkg/gitops"
)

// ScoutReconciler reconciles a Scout object
type ScoutReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=scout.sailr.co,resources=scouts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=scout.sailr.co,resources=scouts/status,verbs=get;update;patch

// Reconcile handles the reconciliation logic for deployed scouts
func (r *ScoutReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	_ = r.Log.WithValues("scout", req.NamespacedName)
	var scout gitopsv1alpha1.Scout

	// your logic here
	err := r.Client.Get(ctx, req.NamespacedName, &scout)
	if err != nil {
		log.Println("error fetching client")
	}

	gitops.InitGitOps(scout.Spec.GitOpsURI)

	return ctrl.Result{}, nil
}

// SetupWithManager handles manager setup for the controller
func (r *ScoutReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitopsv1alpha1.Scout{}).
		Complete(r)
}
