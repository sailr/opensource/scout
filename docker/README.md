# Docker


## Overview

This directory is loaded into the container image at runtime and moved in the file system such that the scripts are available for global access. This allows us to interact with git/bash natively through their respective interfaces rather than relying on a 3rd party library.

This method also gives us a separation of concerns between application code and operations code within the gitops repo.


* init_gitops - instantiates the GitOps repo based on defaults. 
