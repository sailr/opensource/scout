
## 0.0.7 [11-19-2019]

* Bug fixes and performance improvements

See commit 7ec8fa4

---

## 0.0.6 [11-15-2019]

* Bug fixes and performance improvements

See commit 61b7b53

---

## 0.0.5 [11-12-2019]

* Bug fixes and performance improvements

See commit b9d943e

---

## 0.0.4 [11-12-2019]

* Bug fixes and performance improvements

See commit 79aaaf7

---

## 0.0.3 [11-11-2019]

* Bug fixes and performance improvements

See commit 1709892

---

## 0.0.2 [11-11-2019]

* Bug fixes and performance improvements

See commit 6b9fe9b

---
