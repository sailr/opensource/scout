# Scout

The minimal GitOps Kubernetes Operator. Built with [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)

## Forward

The GitOps model is still relatively new and largely liquid from its inception at WeaveWorks; so we'll first define what we mean by "GitOps":

![GitOps](./scout.png)

GitOps is the process by which state is housed in a central "GitOps Repository". This Repository captures the *current state of the world* (in terms of applications running in a kubernetes cluster.).

There are two primary goals of "GitOps":

* Disaster Recovery
* Application Observability (within the cluster)

Side effects of this implementation yield a versioned history of how and when the cluster is/was updated in a push (as opposed to a pull) format. In addition to the enablement of ChatOps provisioning.

## Overview

`ScoutCRD`:

* Github/Gitlab token (git token used to init and update - requires read/write)

`Scout.yml`:

* version: string (git short sha)
* GitOps repository url

Notes:

*GitOps repo declared in `scout.yml` to allow for multiple (per individual, per team, per org GitOps repo)*

*Deploy with kapp to allow for `--dry-run` before full deployment*

Design:

Reconciliation happens as a series of steps. While this process is "hook-able" to a CI/CD tool like [kit](https://gitlab.com/sailr/opensource/kit), we'll focus on just the Scout (GitOps) portion of that flow.

*Prerequisite: scout controller and CRD are deployed on a cluster*

User deploys their application with a [scout.yml](./config/../../config/samples/gitops_v1alpha1_scout.yaml)

When the scout controller encounters an application deployed with scout, it inits a GitOps repo based on the passed in field in the `scout.yml`. If the GitOps repo doesn't exist, it creates it. If it does exist, scout looks for a `projects.yml`. If it can't find one, scout creates one. 

Scout then checks the version in the `scout.yml`. If that field is blank, scout checks the GitOps repo for a previous version. If it doesn't find one, it clones the project in the GitOps repo (adding it as a git submodule). Scout takes the version from the git submodule and applies it back to the `scout.yml` in kubernetes.

After checking out the submodule in the GitOps repo, a pipeline will be kicked off (if Gitlab). That pipeline will tag and version the GitOps repo kicking off its history. We do not commit back to the application repo to prevent an infinite loop of CI/CD commits.

Deployment manifests are checked out to a section of the GitOps repo where they are tracked outside of the application repo (application repo houses base configuration). This also allows for kustomization use.

github/gitlab tokens can be generated through the user setting's menu. Once a token is created, a [secret](https://kubernetes.io/docs/concepts/configuration/secret/#service-accounts-automatically-create-and-attach-secrets-with-api-credentials) should be generated in the cluster. The name of the secret should be `scout`.

`kubectl create secret generic scout --from-literal=token=abc123`




Open Questions:

* How are applications updated via the operator?

Structs:

* ScoutReconciler - the reconciliation object
* ScoutSpec - defines the desired state of each Scout
* ScoutStatus - defines the observed state of each Scout
* Scout - schema for scout's API
* ScoutList - a list of Scouts

## Resources

* [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)
* [kubebuilder book](https://book.kubebuilder.io/)
* [flux](https://github.com/fluxcd/flux)
* [ArgoCD Best Practices](https://blog.argoproj.io/5-gitops-best-practices-d95cb0cbe9ff)
