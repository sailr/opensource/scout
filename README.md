# Scout

The minimal GitOps Kubernetes Operator

## Installation

## Getting started

## Developing

* Clone this repo and run make
* Run `make run deploy` to apply the CRD to your own Kubernetes cluster
* Find the `manager` container in the `scout-system` namespace using a command like `kubectl logs -f -n scout-system pod/scout-controller-manager-77654db9f4-v5j4w manager`
* Apply the sample resource `kubectl apply -f samples/scout_v1alpha1_scout.yml`
* Watch the logs.

## Overview

An overview of scout's implementation can be found in the [SPEC](docs/SPEC.md)
