package gitops

import (
	"log"
	"time"
)

// LifecycleManager manges the gitops lifecycle for scout
func LifecycleManager(gitopsURI, projectURI, version string) {
	// check that the gitops repo exists
}

// ControlLoopStart start or restart the control loop
func ControlLoopStart() {}

// ControlLoopStop stop or pause the control loop
func ControlLoopStop() {}

// ControlLoop ensures that changes are tracked in a 7 minute cadence
func ControlLoop(stop bool) {
	// 7 minute intervals
	ticker := time.NewTicker(420 * time.Second)

	if stop == true {
		log.Println("stopping ticker")
		ticker.Stop()
	}

}
