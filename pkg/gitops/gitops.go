package gitops

import (
	"errors"
	"log"
	"os"
	"os/exec"
	"strings"
)

// InitGitOps checks whether a project exists in the GitOps Repo; if it doesn't
// it creates the gitops repo
func InitGitOps(gitopsuri string) {
	projectName, e := getProjectName(gitopsuri)
	if e != nil {
		log.Printf("project_name errored with: %v", e)
	}
	err := exec.Command("init_gitops", gitopsuri, projectName).Run()
	if err != nil {
		log.Printf("gitops_init errored with: %v", err)
	}
	log.Printf("GitOps init complete")
}

// getProjectName take a project URI and return the project's name
func getProjectName(projectURI string) (string, error) {
	if len(projectURI) == 0 {
		return "", errors.New("empty project name")
	}
	projectNameList := strings.Split(projectURI, "/")
	projectName := projectNameList[len(projectNameList)-1]
	return strings.TrimSuffix(projectName, ".git"), nil
}

// checkExistingGitOpsRepo checks for a preexisting GitOps repo
func checkExistingGitOpsRepo(gitopsuri string) bool {
	projectName, e := getProjectName(gitopsuri)
	if e != nil {
		log.Printf("project_name errored with: %v", e)
	}

	if _, err := os.Stat("/tmp/" + projectName); os.IsNotExist(err) {
		return false
	}
	return true
}

// CheckoutProjectSubmodule checks out the project uri as a submodule to the gitops repo
func CheckoutProjectSubmodule(projecturi string) {}

// CommitToGitOpsRepo commits back to the GitOps repo
func CommitToGitOpsRepo() {}
