package gitops

import (
	"testing"
)

func TestGetProjectNameBasic(t *testing.T) {
	gitopsURI := "git@gitlab.com:jstone28/golang-alpine.git"

	expected := "variant-search-coding-assignment"
	actual, err := getProjectName(gitopsURI)

	if actual != expected {
		t.Log("project names did not match")
		t.Fail()
	} else if err != nil {
		t.Log("error is not empty")
		t.Fail()
	}
}

func TestGetProjectNameEmpty(t *testing.T) {
	gitopsURI := ""

	expected := ""
	actual, err := getProjectName(gitopsURI)

	if actual != expected {
		t.Log("project names did not match")
		t.Fail()
	} else if err == nil {
		t.Log("error should be populated")
		t.Fail()
	}
}
