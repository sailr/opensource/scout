FROM golang:1.12.5 as builder

WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY main.go main.go
COPY api/ api/
COPY pkg/ pkg/
COPY controllers/ controllers/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o manager main.go

FROM alpine
WORKDIR /
RUN apk add --update git bash
COPY docker/ /usr/bin
COPY --from=builder /workspace/manager .

CMD ["/manager"]
